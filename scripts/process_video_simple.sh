#!/bin/bash

#now we transcode the concatenated video to .mp4 and .ogg
nice ffmpeg -i $1 -vcodec libx264 -acodec libfaac -vb 900k -ab 128k -ar 44100 -ac 2 -s $2x$3 -y $5.mp4 &> $5.mp4.log 2>&1
nice ffmpeg2theora $1 -V 900 -A 128k -c 2 -x $2 -y $3 -o $5.ogg &> $5.ogg.log 2>&1 

#and we grab a screenshot from the middle of the video
nice ffmpeg -i $1 -ss $4 -vcodec mjpeg -vframes 1 -an -f rawvideo -s $2x$3 -y $5.jpg &> $5.jpg.log 2>&1

exit 0


