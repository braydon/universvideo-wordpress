#!/bin/bash

#resize and crop our bumper video
viddir=`dirname $5`
videoratio=`echo "scale=4; $6 / $7" | bc`
vr=$(($6 * 100 / $7 * 100))
br=$((640 * 100 / 480 * 100))
if [ $br -gt $vr ] ; then
    thewidth=$(( $6 * 100 / $7 * 100 * 480 * 100 / 1000000 ))
    croptotal=$((640 - $thewidth))
    crop=$(($croptotal / 4 * 2))
    nice ffmpeg -i $viddir/bumper.mpg -r 25 -vb 1024k -ab 192k -ar 44100 -ac 2 -y -cropleft $crop -cropright $crop -s $6x$7 -aspect $videoratio $viddir/bumper_$6x$7.mpg &> $5.bumper.log 2>&1
else
    theheight=$(( $7 * 100 / $6 * 100 * 640 * 100 / 1000000 ))
    croptotal=$((480 - $theheight))
    crop=$(($croptotal / 4 * 2))
    nice ffmpeg -i $viddir/bumper.mpg -r 25 -vb 1024k -ab 192k -ar 44100 -ac 2 -y -croptop $crop -cropbottom $crop -s $6x$7 -aspect $videoratio $viddir/bumper_$6x$7.mpg &> $5.bumper.log 2>&1
fi

#convert our video to mpg so we can concat it with the bumper
nice ffmpeg -i $1 -r 25 -vb 1024k -ab 192k -ar 44100 -ac 2 -y $5_main.mpg &> $5.mpg.log 2>&1

#concatenate our bumper and video
nice cat $viddir/bumper_$6x$7.mpg $5_main.mpg | ffmpeg -y -i - -vcodec copy -acodec copy $5_cat.mpg &> $5.concat.log 2>&1

#and clean up our mess
rm -f $5_main.mpg $viddir/bumper_$6x$7.mpg $5.mpg.log $5.bumper.log

#now we transcode the concatenated video to .mp4 and .ogg
nice ffmpeg -i $5_cat.mpg -vcodec libx264 -acodec libfaac -vb 900k -ab 128k -ar 44100 -ac 2 -s $2x$3 -y $5.mp4 &> $5.mp4.log 2>&1
nice ffmpeg2theora $5_cat.mpg -V 900 -A 128k -c 2 -x $2 -y $3 -o $5.ogg &> $5.ogg.log 2>&1 

#and clean up our mess
rm -f $5_cat.mpg $5.concat.log

#and we grab a screenshot from the middle of the video
nice ffmpeg -i $1 -ss $4 -vcodec mjpeg -vframes 1 -an -f rawvideo -s $2x$3 -y $5.jpg &> $5.jpg.log 2>&1

exit 0


