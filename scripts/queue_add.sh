#!/bin/bash

#todo: make sure there are 5 args, and sanity checks
s=" "
questring=$1$s$2$s$3$s$4$s$5$s$7$s$8

#read the queue file
que_filename='queue'
que_file=$6$que_filename
exec 6<$que_file
let count=1

#create an array for each line
declare -a QUEUE
while read LINE <&6; do
    QUEUE[$count]=$LINE
    ((count++))
done
exec 6>&-
COUNT=1
INQUEUE=0

#check to see that the item to be added isn't already in the queue
until [ $COUNT -gt ${#QUEUE[@]} ]; do
  if [ "${QUEUE[COUNT]}" = $questring ] ; then
      INQUEUE=1
  fi
  let COUNT=COUNT+1
done

#read the running file 
running_filename=running
exec 10<$6$running_filename
read LINE <&10
exec 10>&-
IFS=" "
set -- $LINE
args=( $LINE )
FILE=${args[0]}


#if the item not in the queue, continue

if [ $INQUEUE -eq 0 ] ; then
    #if also not running, continue
    if [ $FILE != $1 ] ; then
        #add to the bottom of the queue file
        echo $questring >> $que_file
    fi
fi


