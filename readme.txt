=== Media Tags ===
Contributors: Braydon Fuller
Donate link: http://www.braydon.com/blog/
Tags: video, media
Requires at least: 2.8
Tested up to: 2.8
Stable tag: 0.1

== Description ==

Provides ability to transcode/resize (using FFMPEG), and play videos with native support, using the HTML5 video tag and canvas element, in Firefox (Ogg Vorbis/Theora) and Safari (H264/AAC), and plugin support for Shockwave Flash (FLV), with the same Firefox-like controls. It intergrates with Wordpress Media Library so that when you upload a video, it automatically gets resized and transcoded to the configured formats. You can then insert the video into a post by browsing the library and clicking the "Insert into Post" button, which inserts a shortcode which will then be converted to the html needed to play the video. Note: This plugin requires that your server have ffmpeg, ffmpeg2theora, and the nessasary software packages to encode to the codecs ogg, mp3, aac, x264, and flv.

== Installation ==

== Frequently Asked Questions ==

== Screenshots ==

== Version Histroy == 
